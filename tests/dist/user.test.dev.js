"use strict";

var dbHandler = require('./db-handler');

var User = require('../models/User');

beforeAll(function _callee() {
  return regeneratorRuntime.async(function _callee$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.next = 2;
          return regeneratorRuntime.awrap(dbHandler.connect());

        case 2:
        case "end":
          return _context.stop();
      }
    }
  });
});
afterEach(function _callee2() {
  return regeneratorRuntime.async(function _callee2$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          _context2.next = 2;
          return regeneratorRuntime.awrap(dbHandler.clearDatabase());

        case 2:
        case "end":
          return _context2.stop();
      }
    }
  });
});
afterAll(function _callee3() {
  return regeneratorRuntime.async(function _callee3$(_context3) {
    while (1) {
      switch (_context3.prev = _context3.next) {
        case 0:
          _context3.next = 2;
          return regeneratorRuntime.awrap(dbHandler.closeDatabase());

        case 2:
        case "end":
          return _context3.stop();
      }
    }
  });
});
var userComplete = {
  name: 'Kob',
  gender: 'M'
};
var userErrorNameEmpty = {
  name: '',
  gender: 'M'
};
var userErrorName2Alphabets = {
  name: 'Ko',
  gender: 'M'
};
describe('User', function () {
  it('สามารถเพิ่ม user ได้', function _callee4() {
    var error, user;
    return regeneratorRuntime.async(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            error = null;
            _context4.prev = 1;
            user = new User(userComplete);
            _context4.next = 5;
            return regeneratorRuntime.awrap(user.save());

          case 5:
            _context4.next = 10;
            break;

          case 7:
            _context4.prev = 7;
            _context4.t0 = _context4["catch"](1);
            error = _context4.t0;

          case 10:
            expect(error).toBeNull();

          case 11:
          case "end":
            return _context4.stop();
        }
      }
    }, null, null, [[1, 7]]);
  });
  it('ไม่สามารถเพิ่ม user ได้ เพราะ name เป็น ช่องว่าง', function _callee5() {
    var error, user;
    return regeneratorRuntime.async(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            error = {};
            _context5.prev = 1;
            user = new User(userErrorNameEmpty);
            _context5.next = 5;
            return regeneratorRuntime.awrap(user.validate());

          case 5:
            _context5.next = 10;
            break;

          case 7:
            _context5.prev = 7;
            _context5.t0 = _context5["catch"](1);
            error = _context5.t0;

          case 10:
            expect(error).not.toBeNull();

          case 11:
          case "end":
            return _context5.stop();
        }
      }
    }, null, null, [[1, 7]]);
  });
  it('ไม่สามารถเพิ่ม user ได้ เพราะ name เป็น 2 ตัว', function _callee6() {
    var error, user;
    return regeneratorRuntime.async(function _callee6$(_context6) {
      while (1) {
        switch (_context6.prev = _context6.next) {
          case 0:
            error = {};
            _context6.prev = 1;
            user = new User(userErrorName2Alphabets);
            _context6.next = 5;
            return regeneratorRuntime.awrap(user.save());

          case 5:
            _context6.next = 10;
            break;

          case 7:
            _context6.prev = 7;
            _context6.t0 = _context6["catch"](1);
            error = _context6.t0;

          case 10:
            expect(error).not.toBeNull();

          case 11:
          case "end":
            return _context6.stop();
        }
      }
    }, null, null, [[1, 7]]);
  });
});