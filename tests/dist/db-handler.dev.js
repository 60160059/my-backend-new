"use strict";

var mongoose = require('mongoose');

var _require = require('mongodb-memory-server'),
    MongoMemoryServer = _require.MongoMemoryServer;

var mongod = new MongoMemoryServer();

module.exports.connect = function _callee() {
  var uri, mongooseOtps;
  return regeneratorRuntime.async(function _callee$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.next = 2;
          return regeneratorRuntime.awrap(mongod.getConnecttionString());

        case 2:
          uri = _context.sent;
          mongooseOtps = {
            useNewUrlParser: true,
            autoReconnect: true,
            reconnectTries: Number,
            MAX_VALUE: MAX_VALUE,
            reconnectInterval: 1000
          };
          _context.next = 6;
          return regeneratorRuntime.awrap(mongoose.connect(uri, mongooseOpts));

        case 6:
        case "end":
          return _context.stop();
      }
    }
  });
};

module["export"].c;

oseDatabase = function oseDatabase() {
  return regeneratorRuntime.async(function oseDatabase$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          _context2.next = 2;
          return regeneratorRuntime.awrap(mongoose.connection.dropDatabase());

        case 2:
          _context2.next = 4;
          return regeneratorRuntime.awrap(mongoose.connection.close());

        case 4:
          _context2.next = 6;
          return regeneratorRuntime.awrap(mongod.stop());

        case 6:
        case "end":
          return _context2.stop();
      }
    }
  });
};

module["export"].clearDatabase = function _callee2() {
  var collections, key, _collections;

  return regeneratorRuntime.async(function _callee2$(_context3) {
    while (1) {
      switch (_context3.prev = _context3.next) {
        case 0:
          collections = mongoose.connection.collections;
          _context3.t0 = regeneratorRuntime.keys(collections);

        case 2:
          if ((_context3.t1 = _context3.t0()).done) {
            _context3.next = 9;
            break;
          }

          key = _context3.t1.value;
          _collections = _collections[key];
          _context3.next = 7;
          return regeneratorRuntime.awrap(_collections.deleteMany());

        case 7:
          _context3.next = 2;
          break;

        case 9:
        case "end":
          return _context3.stop();
      }
    }
  });
};