"use strict";

var express = require('express');

var router = express.Router();
router.get('/:message', function (req, res, next) {
  var params = req.params;
  res.json({
    message: 'Hello Tar',
    params: params
  });
});
module.exports = router;