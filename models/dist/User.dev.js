"use strict";

var mongoose = require('mongoose');

var Schema = mongoose.Schema;
var userSchema = new mongoose.Schema({
  name: String,
  gender: String
});
module.exports = mongoose.model('User', userSchema);