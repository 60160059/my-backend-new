const mongoose = require('mongoose')
const Schema = mongoose.Schema
const userSchema = new Schema({
  name: {
    type: String,
    require: true,
    minlength: 3,
    unique: true
  },
  gender: {
    type: String,
    enum: ['M', 'F']
  }
})

module.exports = mongoose.model('User', userSchema)